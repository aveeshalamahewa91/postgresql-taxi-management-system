- The database contains tables for taxi, driver, user, trip_details, bill_details, customer_service, feedback, owns, owner_taxi, individual and taxi_service_company.

- The taxi table stores information about taxis such as taxi id, registration number, taxi model, taxi year, taxi type, status and driver id.

- The driver table stores information about drivers such as driver id, first name, last name, gender, contact number, rating and age.

- The user table stores information about users such as user id, first name, last name, contact number, gender, address and taxi id.

- The trip_details table stores information about trips such as trip id, trip date, trip amount, driver id, user id, taxi id, start time and end time.

- The bill_details table stores information about bills such as bill id, bill date, advance amount, discount amount, total amount, user id and trip id.

- The customer_service table stores information about customer service employees such as employee id, first name and last name.

- The feedback table stores information about feedback such as feedback id, message, email, employee id, user id and trip id.

- The owns table stores information about owners and the number of cars they own.

- The owner_taxi table stores information about the taxis owned by an owner.

- The individual table stores information about individual owners.

- The taxi_service_company table stores information about taxi service companies.
