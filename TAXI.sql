PGDMP             
            {            Taxi    15.4    15.4 6    Q           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            R           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            S           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            T           1262    16671    Taxi    DATABASE     �   CREATE DATABASE "Taxi" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';
    DROP DATABASE "Taxi";
                postgres    false            �            1259    16694    bill_details    TABLE     �   CREATE TABLE public.bill_details (
    bill_no integer NOT NULL,
    bill_date date,
    advance_amt numeric(10,2),
    discount_amt numeric(10,2),
    total_amt numeric(10,2),
    usr_id integer,
    trip_id integer
);
     DROP TABLE public.bill_details;
       public         heap    postgres    false            �            1259    16701    customer_service    TABLE     �   CREATE TABLE public.customer_service (
    emp_id integer NOT NULL,
    f_name character varying(20),
    l_name character varying(20)
);
 $   DROP TABLE public.customer_service;
       public         heap    postgres    false            �            1259    16684    driver    TABLE     �   CREATE TABLE public.driver (
    driver_id integer NOT NULL,
    f_name character varying(10),
    l_name character varying(20),
    gender character varying(10),
    conatct_no character varying(20),
    rating integer,
    age integer
);
    DROP TABLE public.driver;
       public         heap    postgres    false            �            1259    16706    feedback    TABLE     �   CREATE TABLE public.feedback (
    fbk_id integer NOT NULL,
    message character varying(140),
    email character varying(50),
    emp_id integer,
    usr_id integer,
    trip_id integer
);
    DROP TABLE public.feedback;
       public         heap    postgres    false            �            1259    16723 
   individual    TABLE     s   CREATE TABLE public.individual (
    ssn integer NOT NULL,
    name character varying(20),
    owner_id integer
);
    DROP TABLE public.individual;
       public         heap    postgres    false            �            1259    16718 
   owner_taxi    TABLE     `   CREATE TABLE public.owner_taxi (
    owner_id integer NOT NULL,
    taxi_id integer NOT NULL
);
    DROP TABLE public.owner_taxi;
       public         heap    postgres    false            �            1259    16713    owns    TABLE     Q   CREATE TABLE public.owns (
    owner_id integer NOT NULL,
    no_cars integer
);
    DROP TABLE public.owns;
       public         heap    postgres    false            �            1259    16672    taxi    TABLE     �   CREATE TABLE public.taxi (
    taxi_id integer NOT NULL,
    registration_no character varying(20),
    taxi_model character varying(20),
    taxi_year date,
    taxi_type character varying(20),
    status character varying(20),
    driver_id integer
);
    DROP TABLE public.taxi;
       public         heap    postgres    false            �            1259    16728    taxi_service_company    TABLE     �   CREATE TABLE public.taxi_service_company (
    tsc_id integer NOT NULL,
    tsc_name character varying(20),
    owner_id integer
);
 (   DROP TABLE public.taxi_service_company;
       public         heap    postgres    false            �            1259    16689    trip_details    TABLE       CREATE TABLE public.trip_details (
    trip_id integer NOT NULL,
    trip_date date,
    trip_amt numeric(10,2),
    driver_id integer,
    usr_id integer,
    taxi_id integer,
    strt_time timestamp without time zone,
    end_time timestamp without time zone
);
     DROP TABLE public.trip_details;
       public         heap    postgres    false            �            1259    16679    user_tbl    TABLE     �   CREATE TABLE public.user_tbl (
    usr_id integer NOT NULL,
    f_name character varying(20),
    l_name character varying(20),
    contact_no character(10),
    gender character varying(10),
    address character varying(50),
    taxi_id integer
);
    DROP TABLE public.user_tbl;
       public         heap    postgres    false            H          0    16694    bill_details 
   TABLE DATA           q   COPY public.bill_details (bill_no, bill_date, advance_amt, discount_amt, total_amt, usr_id, trip_id) FROM stdin;
    public          postgres    false    218   B       I          0    16701    customer_service 
   TABLE DATA           B   COPY public.customer_service (emp_id, f_name, l_name) FROM stdin;
    public          postgres    false    219   B       F          0    16684    driver 
   TABLE DATA           \   COPY public.driver (driver_id, f_name, l_name, gender, conatct_no, rating, age) FROM stdin;
    public          postgres    false    216   �B       J          0    16706    feedback 
   TABLE DATA           S   COPY public.feedback (fbk_id, message, email, emp_id, usr_id, trip_id) FROM stdin;
    public          postgres    false    220   �C       M          0    16723 
   individual 
   TABLE DATA           9   COPY public.individual (ssn, name, owner_id) FROM stdin;
    public          postgres    false    223   %D       L          0    16718 
   owner_taxi 
   TABLE DATA           7   COPY public.owner_taxi (owner_id, taxi_id) FROM stdin;
    public          postgres    false    222   �D       K          0    16713    owns 
   TABLE DATA           1   COPY public.owns (owner_id, no_cars) FROM stdin;
    public          postgres    false    221   �D       D          0    16672    taxi 
   TABLE DATA           m   COPY public.taxi (taxi_id, registration_no, taxi_model, taxi_year, taxi_type, status, driver_id) FROM stdin;
    public          postgres    false    214   �D       N          0    16728    taxi_service_company 
   TABLE DATA           J   COPY public.taxi_service_company (tsc_id, tsc_name, owner_id) FROM stdin;
    public          postgres    false    224   �E       G          0    16689    trip_details 
   TABLE DATA           u   COPY public.trip_details (trip_id, trip_date, trip_amt, driver_id, usr_id, taxi_id, strt_time, end_time) FROM stdin;
    public          postgres    false    217   MF       E          0    16679    user_tbl 
   TABLE DATA           `   COPY public.user_tbl (usr_id, f_name, l_name, contact_no, gender, address, taxi_id) FROM stdin;
    public          postgres    false    215   �F       �           2606    16698    bill_details bill_details_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.bill_details
    ADD CONSTRAINT bill_details_pkey PRIMARY KEY (bill_no);
 H   ALTER TABLE ONLY public.bill_details DROP CONSTRAINT bill_details_pkey;
       public            postgres    false    218            �           2606    16700 %   bill_details bill_details_trip_id_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.bill_details
    ADD CONSTRAINT bill_details_trip_id_key UNIQUE (trip_id);
 O   ALTER TABLE ONLY public.bill_details DROP CONSTRAINT bill_details_trip_id_key;
       public            postgres    false    218            �           2606    16705 &   customer_service customer_service_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.customer_service
    ADD CONSTRAINT customer_service_pkey PRIMARY KEY (emp_id);
 P   ALTER TABLE ONLY public.customer_service DROP CONSTRAINT customer_service_pkey;
       public            postgres    false    219            �           2606    16688    driver driver_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.driver
    ADD CONSTRAINT driver_pkey PRIMARY KEY (driver_id);
 <   ALTER TABLE ONLY public.driver DROP CONSTRAINT driver_pkey;
       public            postgres    false    216            �           2606    16712    feedback feedback_emp_id_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_emp_id_key UNIQUE (emp_id);
 F   ALTER TABLE ONLY public.feedback DROP CONSTRAINT feedback_emp_id_key;
       public            postgres    false    220            �           2606    16710    feedback feedback_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (fbk_id);
 @   ALTER TABLE ONLY public.feedback DROP CONSTRAINT feedback_pkey;
       public            postgres    false    220            �           2606    16727    individual individual_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.individual
    ADD CONSTRAINT individual_pkey PRIMARY KEY (ssn);
 D   ALTER TABLE ONLY public.individual DROP CONSTRAINT individual_pkey;
       public            postgres    false    223            �           2606    16722    owner_taxi owner_taxi_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.owner_taxi
    ADD CONSTRAINT owner_taxi_pkey PRIMARY KEY (owner_id, taxi_id);
 D   ALTER TABLE ONLY public.owner_taxi DROP CONSTRAINT owner_taxi_pkey;
       public            postgres    false    222    222            �           2606    16717    owns owns_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.owns
    ADD CONSTRAINT owns_pkey PRIMARY KEY (owner_id);
 8   ALTER TABLE ONLY public.owns DROP CONSTRAINT owns_pkey;
       public            postgres    false    221            �           2606    16676    taxi taxi_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.taxi
    ADD CONSTRAINT taxi_pkey PRIMARY KEY (taxi_id);
 8   ALTER TABLE ONLY public.taxi DROP CONSTRAINT taxi_pkey;
       public            postgres    false    214            �           2606    16678    taxi taxi_registration_no_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.taxi
    ADD CONSTRAINT taxi_registration_no_key UNIQUE (registration_no);
 G   ALTER TABLE ONLY public.taxi DROP CONSTRAINT taxi_registration_no_key;
       public            postgres    false    214            �           2606    16732 .   taxi_service_company taxi_service_company_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.taxi_service_company
    ADD CONSTRAINT taxi_service_company_pkey PRIMARY KEY (tsc_id);
 X   ALTER TABLE ONLY public.taxi_service_company DROP CONSTRAINT taxi_service_company_pkey;
       public            postgres    false    224            �           2606    16693    trip_details trip_details_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.trip_details
    ADD CONSTRAINT trip_details_pkey PRIMARY KEY (trip_id);
 H   ALTER TABLE ONLY public.trip_details DROP CONSTRAINT trip_details_pkey;
       public            postgres    false    217            �           2606    16683    user_tbl user_tbl_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_tbl
    ADD CONSTRAINT user_tbl_pkey PRIMARY KEY (usr_id);
 @   ALTER TABLE ONLY public.user_tbl DROP CONSTRAINT user_tbl_pkey;
       public            postgres    false    215            �           2606    16758    bill_details fkbdtd    FK CONSTRAINT     �   ALTER TABLE ONLY public.bill_details
    ADD CONSTRAINT fkbdtd FOREIGN KEY (trip_id) REFERENCES public.trip_details(trip_id) ON DELETE CASCADE;
 =   ALTER TABLE ONLY public.bill_details DROP CONSTRAINT fkbdtd;
       public          postgres    false    3221    217    218            �           2606    16763    bill_details fkbdusr    FK CONSTRAINT     �   ALTER TABLE ONLY public.bill_details
    ADD CONSTRAINT fkbdusr FOREIGN KEY (usr_id) REFERENCES public.user_tbl(usr_id) ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.bill_details DROP CONSTRAINT fkbdusr;
       public          postgres    false    3217    215    218            �           2606    16793    individual fkeinowns    FK CONSTRAINT     �   ALTER TABLE ONLY public.individual
    ADD CONSTRAINT fkeinowns FOREIGN KEY (owner_id) REFERENCES public.owns(owner_id) ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.individual DROP CONSTRAINT fkeinowns;
       public          postgres    false    221    223    3233            �           2606    16788    owner_taxi fkeowowns    FK CONSTRAINT     �   ALTER TABLE ONLY public.owner_taxi
    ADD CONSTRAINT fkeowowns FOREIGN KEY (owner_id) REFERENCES public.owns(owner_id) ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.owner_taxi DROP CONSTRAINT fkeowowns;
       public          postgres    false    222    3233    221            �           2606    16783    owner_taxi fkeowtax    FK CONSTRAINT     �   ALTER TABLE ONLY public.owner_taxi
    ADD CONSTRAINT fkeowtax FOREIGN KEY (taxi_id) REFERENCES public.taxi(taxi_id) ON DELETE CASCADE;
 =   ALTER TABLE ONLY public.owner_taxi DROP CONSTRAINT fkeowtax;
       public          postgres    false    222    214    3213            �           2606    16733    taxi fketadr    FK CONSTRAINT     �   ALTER TABLE ONLY public.taxi
    ADD CONSTRAINT fketadr FOREIGN KEY (driver_id) REFERENCES public.driver(driver_id) ON DELETE CASCADE;
 6   ALTER TABLE ONLY public.taxi DROP CONSTRAINT fketadr;
       public          postgres    false    216    3219    214            �           2606    16798    taxi_service_company fketscowns    FK CONSTRAINT     �   ALTER TABLE ONLY public.taxi_service_company
    ADD CONSTRAINT fketscowns FOREIGN KEY (owner_id) REFERENCES public.owns(owner_id) ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.taxi_service_company DROP CONSTRAINT fketscowns;
       public          postgres    false    221    224    3233            �           2606    16768    feedback fkfbemp    FK CONSTRAINT     �   ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT fkfbemp FOREIGN KEY (emp_id) REFERENCES public.customer_service(emp_id) ON DELETE CASCADE;
 :   ALTER TABLE ONLY public.feedback DROP CONSTRAINT fkfbemp;
       public          postgres    false    3227    220    219            �           2606    16773    feedback fkfbtd    FK CONSTRAINT     �   ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT fkfbtd FOREIGN KEY (trip_id) REFERENCES public.trip_details(trip_id) ON DELETE CASCADE;
 9   ALTER TABLE ONLY public.feedback DROP CONSTRAINT fkfbtd;
       public          postgres    false    220    217    3221            �           2606    16778    feedback fkfbusr    FK CONSTRAINT     �   ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT fkfbusr FOREIGN KEY (usr_id) REFERENCES public.user_tbl(usr_id) ON DELETE CASCADE;
 :   ALTER TABLE ONLY public.feedback DROP CONSTRAINT fkfbusr;
       public          postgres    false    215    220    3217            �           2606    16743    trip_details fktddr    FK CONSTRAINT     �   ALTER TABLE ONLY public.trip_details
    ADD CONSTRAINT fktddr FOREIGN KEY (driver_id) REFERENCES public.driver(driver_id) ON DELETE CASCADE;
 =   ALTER TABLE ONLY public.trip_details DROP CONSTRAINT fktddr;
       public          postgres    false    217    216    3219            �           2606    16753    trip_details fktdtax    FK CONSTRAINT     �   ALTER TABLE ONLY public.trip_details
    ADD CONSTRAINT fktdtax FOREIGN KEY (taxi_id) REFERENCES public.taxi(taxi_id) ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.trip_details DROP CONSTRAINT fktdtax;
       public          postgres    false    3213    214    217            �           2606    16748    trip_details fktdusr    FK CONSTRAINT     �   ALTER TABLE ONLY public.trip_details
    ADD CONSTRAINT fktdusr FOREIGN KEY (usr_id) REFERENCES public.user_tbl(usr_id) ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.trip_details DROP CONSTRAINT fktdusr;
       public          postgres    false    215    217    3217            �           2606    16738    user_tbl fkusta    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_tbl
    ADD CONSTRAINT fkusta FOREIGN KEY (taxi_id) REFERENCES public.taxi(taxi_id) ON DELETE CASCADE;
 9   ALTER TABLE ONLY public.user_tbl DROP CONSTRAINT fkusta;
       public          postgres    false    215    3213    214            H   j   x�-��	AC��%���&n�We@"&c�pX 5��f���-�!:�!� �*�����k ^q�Z�e�أ��o��vll�߮ �P���L:rV
�����i�      I   W   x��;
�0E���b�
���#ɸL}���U��>%2�+e@85�ƌ�J�&3^:7��UF��6<,%Ʉ��Ža����eN      F   �   x�E�;�0����1�Fۑ�Edpq�r�P������m=y�=��j��}C���g��������L���0u�P���(#���pL�sz��S���Α�#	��B�1+!�)-C��dL0���(cW�qe솕y�E8&Z 縰�T�ϐA���G�Ʒ�~��)������>8      J   y   x�M�A� ����Sx#�]�Ew=���!�)&�����/�Q�E,f���>!���!��$�id9�"��V�wG���MX��Ņۉ�F���bt��|p�0���x���欇A5`�����1�      M   X   x�M�1
�0 �9yE_ �Mq.~�%b���Q����q���T+�����]�'8��܁������ "���ʯ{$�0�=�Ӏ뀈���      L   &   x�3�4�2�4�2�4b.NS.SN3 6����� Dt      K      x�3�4�2bcN#. m
�c���� '��      D   �   x�e�]K�0��O~E�����k��'�ě�e�Z��D�ɨ� ��yx��s�A)���z�ZH΁8r��O�0��pp�����(IZh�����j�u���LB]��+$�,TC�s�2\B�-�$�߹~��rEm�(C���3��*a��p���p*n�e:[�l�H�`�ct1���u������I����ɋ�����(������k@&6!�����-��T���1�~ѳX      N   V   x�3�t�M�KIT(I��TH��-�4�2���/�/B3�2�tO�K-�LNF6�2�tI�)A�n�e�阔�Y���v	r��qqq ��!�      G   �   x�U��� Dѳ���fl�FԒ��XLȆ���@Q�p\���y=�u�>^v;oK5tP��;�Ф�m�*ܦ��(x(
΢*j5
q���m�ٳA�2-�8�|�L�����|m��V�B̂���3ǆ���H)}θ9�      E   �   x�u�1�0�����z�^�� h 7�$�N�{K��8�����w��ھ�J��Y#�-;��c�q	w�'������݃��۫�-J�Z�}I�I�@�)v�8Q)y��#I(�Ir���I��슶��R7}�6J���2E!ɒ��f�
Ш��ۺ'�BP�#m���O��T�D�     